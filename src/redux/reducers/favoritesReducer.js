import {
  ADD_PRODUCT_TO_FAVORITES_ACTION_TYPE,
  DEL_PRODUCT_FROM_FAVORITES_ACTION_TYPE,
  SAVE_FAVORITES_TO_LOCAL_STORAGE,
  GET_FAVORITES_LOCAL_STORAGE,
} from "../actions";

const like = [];
export function favoritesReducer(state = like, action) {
  switch (action.type) {
    case ADD_PRODUCT_TO_FAVORITES_ACTION_TYPE:
      return [...state, action.payload.productId];
    case DEL_PRODUCT_FROM_FAVORITES_ACTION_TYPE:
      return state.filter(
        (productId) => productId !== action.payload.productId
      );

    case SAVE_FAVORITES_TO_LOCAL_STORAGE:
      return [...state];
    case GET_FAVORITES_LOCAL_STORAGE:
      return action.payload;
    default:
      return state;
  }
}
