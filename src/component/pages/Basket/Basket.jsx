import React from "react";
import Form from "../../Form/Form";
import {
  ADD_TO_BASKET_VALUE_ACTION_TYPE,
  REMOVE_VALUE_FROM_BASKET_ACTION_TYPE,
  REMOVE_FROM_BASKET_ACTION_TYPE,
  MODAL_THERE_VISIBLE_ACTION_TYPE,
  MODAL_THERE_INVISIBLE_ACTION_TYPE,
  GET_STATE_ARTICLE,
  CLEAR_STATE_ARTICLE,
  CLOTHE_FORM_SECOND,
  delProductFromFavorites,
  addProductToFavorites,
  OPEN_FORM_SECOND,
} from "../../../redux/actions";
import {
  favoritesSelector,
  formVisibilitySecond,
  modalThere,
  selectedId,
} from "../../../redux/selectors";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "../../Button/Button";
import { Card } from "../../Card/Card";
import { Modal } from "../../Modal/Modal";
import { Svg } from "../../Button/Svg";
import "./basket.scss";

function Basket() {
  const products = useSelector((state) => state.products.products);
  const favorites = useSelector(favoritesSelector);
  const basket = useSelector((state) => state.basket);
  const modal = useSelector(modalThere);
  const articleCard = useSelector(selectedId);
  const formSecond = useSelector(formVisibilitySecond);
  const dispatch = useDispatch();

  const addToLike = (productId) => {
    favorites.includes(productId)
      ? dispatch(delProductFromFavorites(productId))
      : dispatch(addProductToFavorites(productId));
  };

  const openModalDeleteProduct = (article) => {
    dispatch({
      type: MODAL_THERE_VISIBLE_ACTION_TYPE,
    });
    dispatch({
      type: GET_STATE_ARTICLE,
      payload: article,
    });
  };

  const buyEverything = (props) => {
    dispatch({
      type: OPEN_FORM_SECOND,
    });
  };
  const deleteProduct = () => {
    dispatch({
      type: REMOVE_FROM_BASKET_ACTION_TYPE,
      payload: articleCard,
    });
    dispatch({ type: CLEAR_STATE_ARTICLE });
  };

  const clothFormSecond = () => {
    dispatch({
      type: CLOTHE_FORM_SECOND,
    });
  };
  const quantity = (productId) => basket[productId];
  function incrementBasket(article) {
    dispatch({ type: ADD_TO_BASKET_VALUE_ACTION_TYPE, payload: article });
  }
  function decrementBasket(article) {
    dispatch({
      type: GET_STATE_ARTICLE,
      payload: article,
    });
    basket[article] === 1
      ? dispatch({
          type: MODAL_THERE_VISIBLE_ACTION_TYPE,
        })
      : dispatch({
          type: REMOVE_VALUE_FROM_BASKET_ACTION_TYPE,
          payload: article,
        });
  }

  if (products.length === 0) {
    return null;
  }

  const basketProducts = Object.entries(basket).map(([productId, quantity]) => {
    const product = products.find((product) => {
      return product.article === productId;
    });

    return product;
  });

  return (
    <>
      {Object.keys(basket).length === 0 ? (
        <h2>Корзина порожня. Додайте будьласка товар</h2>
      ) : (
        <ul className="basket">
          {basketProducts.map((el) => (
            <Card
              className="basket__list"
              product={el}
              key={el.article}
              name={el.name}
              img={el.img}
              prise={el.prise}
              color={el.color}
              article={el.article}
              action={
                <>
                  <div>
                    <Button
                      text="+"
                      handleClick={() => incrementBasket(el.article)}
                    />
                    <p className="basket__prise">{quantity(el.article)}</p>
                    <Button
                      text="-"
                      handleClick={() => decrementBasket(el.article)}
                    />
                  </div>
                  <Button
                    class="basket__btn--favorite"
                    text=<Svg
                      color={favorites.includes(el.article) ? "red" : "white"}
                    />
                    handleClick={() => addToLike(el.article)}
                  />

                  <p className=" basket__sum">
                    Вартість: {quantity(el.article) * el.prise}
                  </p>

                  <div
                    onClick={() => {
                      openModalDeleteProduct(el.article);
                    }}
                    className="basket__clothe"
                  />
                </>
              }
            />
          ))}
          <li>
            <button className=" basket__byAll" onClick={buyEverything}>
              Купити обране
            </button>
          </li>
        </ul>
      )}
      {modal && (
        <Modal
          closeButton={true}
          header="Видалити товар з кошику ?"
          text="Click ok  to delete or cancel if you change your mind "
          wrapperClose={(e) => e.stopPropagation()}
          onClose={() => {
            dispatch({
              type: MODAL_THERE_INVISIBLE_ACTION_TYPE,
            });
          }}
          action={
            <>
              <Button
                text="Ok"
                handleClick={() => {
                  deleteProduct();
                  dispatch({
                    type: MODAL_THERE_INVISIBLE_ACTION_TYPE,
                  });
                }}
              />
              <Button
                text="Cancel"
                handleClick={() => {
                  dispatch({
                    type: MODAL_THERE_INVISIBLE_ACTION_TYPE,
                  });
                }}
              />
            </>
          }
        />
      )}

      {formSecond && (
        <Form
          wrapperClose={(e) => e.stopPropagation()}
          text=" Купити весь обраний товар"
          class="basket__wrapper"
          classForm="basket__form"
          classInput="basket__input"
          clotheHandel={clothFormSecond}
          buttonStyle="basket__btn"
        />
      )}
    </>
  );
}

export default Basket;
