import React from "react";
import PropTypes from "prop-types";
import "./modal.scss";

export function Modal(props) {
  return (
    <div
      className={props.bakGround}
      onClick={() => {
        props.onClose();
      }}
    >
      <div className={props.wrapper} onClick={props.wrapperClose}>
        <h1 className={props.headerText}>{props.header}</h1>
        {props.closeButton && (
          <div
            onClick={() => {
              props.onClose();
            }}
            className={props.btvCloth}
          />
        )}
        <p className={props.bodyText}>{props.text}</p>
        <div>{props.action}</div>
      </div>
    </div>
  );
}

Modal.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
};
Modal.defaultProps = {};
