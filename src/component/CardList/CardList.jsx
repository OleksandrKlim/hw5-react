import React from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  addProductToFavorites,
  delProductFromFavorites,
} from "../../redux/actions";
import { Card } from "../Card/Card";
import { Button } from "../Button/Button";
import { Svg } from "../Button/Svg";
import { Modal } from "../Modal/Modal";

import {
  selectedId,
  favoritesSelector,
  modalFirst,
  modalSecond,
  basketSelector,
  loading,
} from "../../redux/selectors";
import {
  MODAL_FIRST_INVISIBLE_ACTION_TYPE,
  MODAL_SECOND_VISIBLE_ACTION_TYPE,
  MODAL_SECOND_INVISIBLE_ACTION_TYPE,
  MODAL_FIRST_VISIBLE_ACTION_TYPE,
  ADD_TO_BASKET_ACTION_TYPE,
  ADD_TO_BASKET_VALUE_ACTION_TYPE,
  REMOVE_FROM_BASKET_ACTION_TYPE,
  CLEAR_STATE_ARTICLE,
  GET_STATE_ARTICLE,
} from "../../redux/actions";

import "./cardList.scss";

export function CardList() {
  const favorites = useSelector(favoritesSelector);
  const firstModal = useSelector(modalFirst);
  const secondModal = useSelector(modalSecond);
  const articleCard = useSelector(selectedId);
  const basket = useSelector(basketSelector);
  const isLoading = useSelector(loading);
  const dispatch = useDispatch();

  const addToLike = (productId) => {
    favorites.includes(productId)
      ? dispatch(delProductFromFavorites(productId))
      : dispatch(addProductToFavorites(productId));
  };

  const addToBuyClick = (productId) => {
    dispatch({
      type: GET_STATE_ARTICLE,
      payload: productId,
    });
    basket.hasOwnProperty(productId)
      ? dispatch({
          type: MODAL_SECOND_VISIBLE_ACTION_TYPE,
        })
      : dispatch({
          type: MODAL_FIRST_VISIBLE_ACTION_TYPE,
        });
  };

  const products = useSelector((state) => state.products.products);

  return (
    <>
      {isLoading && <h2>Loading</h2>}
      <ul className="cardList">
        {products.map((product) => (
          <Card
            key={product.article}
            name={product.name}
            img={product.img}
            prise={product.prise}
            color={product.color}
            article={product.article}
            action={
              <>
                <Button
                  class="cardList__btn"
                  text="Add to cart"
                  handleClick={() => addToBuyClick(product.article)}
                />

                <Button
                  class="cardList__btn--favorite"
                  text=<Svg
                    color={
                      favorites.includes(product.article) ? "red" : "white"
                    }
                  />
                  handleClick={() => {
                    addToLike(product.article);
                  }}
                />
              </>
            }
          />
        ))}
      </ul>
      {firstModal && (
        <Modal
          closeButton={true}
          header="Add purchase to cart ?"
          text="Click ok to add or cancel if you change your mind"
          wrapperClose={(e) => e.stopPropagation()}
          onClose={() => {
            dispatch({ type: MODAL_FIRST_INVISIBLE_ACTION_TYPE });
            dispatch({ type: CLEAR_STATE_ARTICLE });
          }}
          action={
            <>
              <Button
                text="Ok"
                handleClick={() => {
                  dispatch({
                    type: ADD_TO_BASKET_ACTION_TYPE,
                    payload: articleCard,
                  });
                  dispatch({ type: CLEAR_STATE_ARTICLE });
                  dispatch({ type: MODAL_SECOND_INVISIBLE_ACTION_TYPE });
                }}
              />
              <Button
                text="Cancel"
                handleClick={() => {
                  dispatch({ type: MODAL_FIRST_INVISIBLE_ACTION_TYPE });
                  dispatch({ type: CLEAR_STATE_ARTICLE });
                }}
              />
            </>
          }
        />
      )}
      {secondModal && (
        <Modal
          closeButton={true}
          header="This item is already in your shopping cart."
          text="Add another one? Click ok to add or delate to remove the item, if you change your mind click the cross"
          wrapperClose={(e) => e.stopPropagation()}
          onClose={() => {
            dispatch({ type: MODAL_SECOND_INVISIBLE_ACTION_TYPE });
            dispatch({ type: CLEAR_STATE_ARTICLE });
          }}
          action={
            <>
              <Button
                text="Ok"
                handleClick={() => {
                  dispatch({
                    type: ADD_TO_BASKET_VALUE_ACTION_TYPE,
                    payload: articleCard,
                  });
                  dispatch({ type: MODAL_SECOND_INVISIBLE_ACTION_TYPE });
                  dispatch({ type: CLEAR_STATE_ARTICLE });
                }}
              />
              <Button
                text="Delete"
                handleClick={() => {
                  dispatch({
                    type: REMOVE_FROM_BASKET_ACTION_TYPE,
                    payload: articleCard,
                  });

                  dispatch({ type: MODAL_SECOND_INVISIBLE_ACTION_TYPE });
                  dispatch({ type: CLEAR_STATE_ARTICLE });
                }}
              />
            </>
          }
        />
      )}
    </>
  );
}
Modal.defaultProps = {
  closeButton: false,
  bakGround: "wrapper",
  wrapper: "modal",
  headerText: "modal__header",
  btvCloth: "modal__cloth",
  bodyText: "modal__bodyText",
};
Button.defaultProps = {
  class: "modal__btn",
};
